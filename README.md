# Sokae project

Rust-like programming language focused on microcontrollers.

The name is inspired on the crab pet of _Rust_ and means crab in _Emberá Chamí_ language.

## Programming structure

```
Sokae source code             API
        |                      |
        +--------+    +--------+
                 |    |
                 v    v
               Sokae to C 
               Transpiler      
                   |
                   v
            Transpiled code    API in C
                   |              |
                   +----+    +----+
                        |    |
                        v    v
                  Native C compiler
                          |
                          v
                     Binary file
```