//Numeric variable definition test
fn main() {
    let var0: bool  = true;     //boolean variable
    let var1: bool  = false;

    let var2: i8    = 129;       //integer variables
    let var3: i64   = -6_835_292;  

    let var4: u8    = 0b0011_0101;   //unsigned integer variables
    let var5: u16   = 0o073452;  
    let var6: u32   = 103_937_465;
    let var7: u64   = 0xAAFF_7625;//_28F5_D47B;

    let var8: f32   = 1_342.56;      //floating point variables
    let var9: f64   = -34.035_440; 

    let var10: char = 'f';    //char and string literals    

    // let var11: usize = 1_345;
    // let var12: isize = -12_367;
}
