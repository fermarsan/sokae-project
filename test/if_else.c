
int main(void)
{
	int8_t var1 = 10;
	int8_t var2 = 1;
	bool logic1 = true;
	
	if( var1 <= var2 )
	{
		logic1 = true;
		var1 += 1;
	}
	
	if( var1 > var2 )
	{
		var1 = 0;
		var2 = 0;
	}
	else
	{
		var1 = -1;
	}

	return 0;
}
