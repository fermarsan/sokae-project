int32_t A = 45;

int main(void)
{
	int16_t b = 67;
	
	if( A == b )
	{
		A += b;
		b = 0;
	}

	return 0;
}
