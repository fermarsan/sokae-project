//arithmetic and logical operation test
fn main() {
    let var1 = 10i8;
    let var2: i8 = 1; 
    let logic1 = true;

    logic1 = (var1 == var2);
    logic1 = (var1 != var2);
    logic1 = (var1 > var2);
    logic1 = (var1 < var2);
    logic1 = (var1 >= var2);
    logic1 = (var1 <= var2);       
}