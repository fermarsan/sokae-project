//Numeric variable definition test
fn main() {
    let var2 = 129i8;       //integer variables
    let var3 = -6_835_292i64;  

    let var4 = 0b0011_0101u8;   //unsigned integer variables
    let var5 = 0o073452u16;  
    let var6 = 103_937_465u32;
    let var7 = 0xAAFF_7625_28F5_D47Bu64;

    let var8 = 1_342.56f32;      //floating point variables
    let var9 = -34.035_440f64; 

    let var10 = -45isize;
    let var11 = 9731usize;

}
