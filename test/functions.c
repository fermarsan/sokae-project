int16_t global_1 = 34;
int16_t global_2 = 125;

int main(void)
{
	int32_t local = 45;
	int64_t local_2;

	return 0;
}

void borra_global_1(void)
{
	global_1 = 0;
}

void borra( int16_t g )
{
	g = 0;
}

int16_t cero(void)
{
	return 0;
}

void suma_1( int16_t v1, int16_t v2 )
{
	global_1 = v1 + v2;
}

int16_t suma_2( int16_t v1, int16_t v2 )
{
	return v1 + v2;
}
