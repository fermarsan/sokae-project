//Numeric variable definition test
fn main() {
    let var0: bool;     //boolean variable

    let var1: i8;       //integer variables
    let var2: i16;  
    let var3: i32;  
    let var4: i64;

    let var5: u8;       //unsigned integer variables
    let var6: u16;  
    let var7: u32;  
    let var8: u64;  

    let var9: f32;      //floating point variables
    let var10: f64; 

    let var11: char;    //char and string literals   
    
    let var12: usize;   //processor's base integers
    let var13: isize;
}
