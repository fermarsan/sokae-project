//Numeric variable definition test
fn main() {
    let var0 = true;     //boolean variable
    let var1 = false;

    let var2 = 1345;     //numeric
    let var3 = 71.4;
    let var4 = -457;
    let var5 = -10.445;

    let var6 = 'd';     //char and string literals    

}
