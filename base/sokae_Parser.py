from sokae_Lexer import sokae_Lexer
from sly import Parser
#import sys, os

class sokae_Parser(Parser):

    debugfile = 'parser.out'

    precedence = (
       ( 'left', '+', '-' ),
       ( 'left', '*', '/', MODULUS),
       ( 'left', '&', '|', '^' ),
       ( 'left', '<', '>' ),
       ( 'right', '=' ),
       ( 'right', '!', UMINUS),
       ( 'right', KW_RETURN),
    )

    tokens = sokae_Lexer.tokens

    def __init__(self): 
        self.error_s = ''       #error stream
        self.output_s = ''      #output stream
        self.exType = ''

        # self.output_header =    """include <stdint.h>
        #                            int main(void){
        #                         """
    
    #guarda los archivos de salida
    def saveOutput(self, name):
        outText = open(name,'w' )
        outText.write( self.output_s )  
        outText.close()
        
        # outText = open(name + '.var', 'w' )
        # for v in self.vars:
        #     outText.write( v + '\t' )
        # outText.close()
        #pass

    def error(self, p):
        if p:
            print( '\nSyntax error in the line ' + str(p.lineno) )
        else:
            print( '\nSyntax error at the end of file' )
        pass

    #************************* Program *************************  
    @_( 'Statements' )    
    def prog(self, p):
        self.output_s += p.Statements

    #--------------- Block comments --------------- 
    @_( 'L_BLK_COMMENT Statements R_BLK_COMMENT' )      
    def Statement(self, p):
        return ''

    #************************* Statements *************************   
    @_( 'Statement', 'Statements Statement' )      
    def Statements(self, p):
        if len(p) == 2:
            return p.Statements + p.Statement
        elif len(p) == 1:
            return p.Statement

    @_( #'";"', 
        'Item', 'LetStatement', 'ExpressionStatement' )
    def Statement(self, p):
        return p[0] + '\n'

    #************************* Items *************************
    @_( 'Function', 'StaticItem' )
    def Item(self, p):
        return p[0]

    #--------------- Static item ---------------
        
    @_( 'KW_STATIC IDENTIFIER ":" Type "=" Expression ";"', #variable declaration and initialization
        'KW_STATIC IDENTIFIER ":" Type ";"' )               #variable declaration
    def StaticItem(self, p):
        self.exType = ''
        if len(p) == 7:
            return p.Type + ' ' + p.IDENTIFIER + ' = ' + p.Expression + ';'
        elif len(p) == 5:
            return p.Type + ' ' + p.IDENTIFIER + ';'        

    #--------------- Functions ---------------
    @_( 'KW_FN IDENTIFIER "(" FunctionParameters ")" "-" ">" Type BlockExpression',
        'KW_FN IDENTIFIER "(" ")" "-" ">" Type BlockExpression',
        'KW_FN IDENTIFIER "(" FunctionParameters ")" BlockExpression',
        'KW_FN IDENTIFIER "(" ")" BlockExpression' )
    def Function(self, p):
        if p.IDENTIFIER == 'main':
            return '\nint main(void)\n' + p.BlockExpression[:-2] + '\n\n\treturn 0;\n}'
        elif len(p) == 9:
            return '\n' + p.Type + ' ' + p.IDENTIFIER + '( ' + p.FunctionParameters + ' )\n' + p.BlockExpression
        elif len(p) == 8:
            return '\n' + p.Type + ' ' + p.IDENTIFIER + '(void)\n' + p.BlockExpression
        elif len(p) == 6:
            return '\nvoid ' + p.IDENTIFIER + '( ' + p.FunctionParameters + ' )\n' + p.BlockExpression
        elif len(p) == 5:
            return '\nvoid ' + p.IDENTIFIER + '(void)\n' + p.BlockExpression

    @_( 'FunctionParameters "," FunctionParam',
        'FunctionParam' )      
    def FunctionParameters(self, p):
        if len(p) == 3: 
            return p.FunctionParameters + ', ' + p.FunctionParam
        elif len(p) == 1:
            return p.FunctionParam

    @_( 'IDENTIFIER ":" Type' )      
    def FunctionParam(self, p):
        return p.Type + ' ' + p.IDENTIFIER

    #--------------- Let statement ---------------
    @_( 'KW_LET IDENTIFIER ":" Type "=" Expression ";"',    #variable declaration and initialization
        'KW_LET IDENTIFIER "=" Expression NumericType ";"', #variable inicialization (suffix annotation)
        'KW_LET IDENTIFIER ":" Type ";"' )                  #variable declaration
    def LetStatement(self, p):
        self.exType = ''
        if len(p) == 7:
            return p.Type + ' ' + p.IDENTIFIER + ' = ' + p.Expression + ';'
        elif len(p) == 6:
            return p.NumericType + ' ' + p.IDENTIFIER + ' = ' + p.Expression + ';'
        elif len(p) == 5:
            return p.Type + ' ' + p.IDENTIFIER + ';'   
                
    @_( 'KW_LET IDENTIFIER "=" Expression ";"' )  #variable inicialization (default types)
    def LetStatement(self, p):
        defaultType = ''
        if self.exType == 'BOOLEAN_LITERAL':
            defaultType = 'bool'    #bool
        elif self.exType == 'FLOAT_LITERAL':
            defaultType = 'float'   #f32
        elif self.exType == 'INTEGER_LITERAL':
            defaultType = 'int16_t' #i16
        elif self.exType == 'CHAR_LITERAL':
            defaultType = 'char'    #char
        self.exType = ''
        return defaultType + ' ' + p.IDENTIFIER + ' = ' + p.Expression + ';'  #i16
    
    #--------------- Expresion statement ---------------
    #@_( 'Expression ";"' )  
    @_( 'ExpressionWithoutBlock ";"', 'ExpressionWithBlock ";"', 'ExpressionWithBlock' )   
    def ExpressionStatement(self, p):
        if len(p) == 2:
            return p[0] + ';'
        elif len(p) == 1:
            return p[0]

    @_( 'error ";"' )   
    def ExpressionStatement(self, p):
        msg = 'Error: Invalid expression. Line ' + str(p.lineno) + '\n'
        self.error_s += msg
        print(msg)

    #************************* Expresions *************************
    @_( 'ExpressionWithoutBlock' )#, 'ExpressionWithBlock' )      
    def Expression(self, p):
        return p[0]

    @_( 'BlockExpression', 'IfExpression', 'LoopExpression' )
    def ExpressionWithBlock(self, p):
        return '\n' + p[0]

    @_( 'InfiniteLoopExpression', 'PredicateLoopExpression',)# 'IteratorLoopExpression' )
    def LoopExpression(self, p):
        return p[0]

    @_( 'KW_LOOP BlockExpression' )
    def InfiniteLoopExpression(self, p):
        return 'while(true)\n' + p.BlockExpression

    @_( 'KW_WHILE Expression BlockExpression' )
    def PredicateLoopExpression(self, p):
        return 'while(' + p.Expression + ')\n' + p.BlockExpression

    @_( 'KW_IF Expression BlockExpression',
        'KW_IF Expression BlockExpression KW_ELSE BlockExpression',
        'KW_IF Expression BlockExpression KW_ELSE IfExpression' )      
    def IfExpression(self, p):
        if len(p) == 3:
            return 'if( ' + p.Expression + ' )\n' + p[2]
        elif len(p) == 5:
            return 'if( ' + p.Expression + ' )\n' + p[2] + '\nelse\n' + p[4]

    @_( '"{" Statements "}"','"{" Statements ExpressionWithoutBlock "}"',
        '"{" ExpressionWithoutBlock "}"' )          
    def BlockExpression(self, p):
        if len(p) == 3:
            if p[1][-1] == '\n':    #if ended in "\n;"
                return '{\n\t' + p[1][:-1].replace( '\n','\n\t' ) + '\n}'
            else:
                return '{\n\t' + p[1].replace( '\n','\n\t' ) + ';\n}'
        elif len(p) == 4:
            return '{\n\t' + (p[1]+p[2]).replace( '\n','\n\t' ) + ';\n}'

    @_( 'IDENTIFIER', 'LiteralExpression', 'OperatorExpression', 'GroupedExpression',
        'ReturnExpression' )      
    def ExpressionWithoutBlock(self, p):
        return p[0]

    @_( 'NegationExpression', 'AssignmentExpression', 'ArithmeticOrLogicalExpression',
        'LazyBooleanExpression', 'ComparisonExpression', 'CompoundAssignmentExpression' )      
    def OperatorExpression(self, p):
        return p[0]

    @_( '"(" Expression ")"' )      
    def GroupedExpression(self, p):
        return p[0] + p[1] + p[2]

    @_( 'Expression "+" Expression', 'Expression "-" Expression',
        'Expression "*" Expression', 'Expression "/" Expression',
        'Expression MODULUS Expression', 'Expression "&" Expression',
        'Expression "|" Expression', 'Expression "^" Expression',
        'Expression "<" "<" Expression', 'Expression ">" ">" Expression' )      
    def ArithmeticOrLogicalExpression(self, p):
        return p[0] + ' ' + p[1] + ' ' + p[2]

    @_( 'Expression "&" "&" Expression', 'Expression "|" "|" Expression' )      
    def LazyBooleanExpression(self, p):
        return p[0] + ' ' + p[1] + p[2] + ' ' + p[3]

    @_( 'Expression "=" "=" Expression', 'Expression "!" "=" Expression',
        'Expression ">" Expression', 'Expression "<" Expression',
        'Expression ">" "=" Expression', 'Expression "<" "=" Expression' )      
    def ComparisonExpression (self, p):
        if len(p) == 3:
            return p[0] + ' ' + p[1] + ' ' + p[2]      
        elif len(p) == 4:
            return p[0] + ' ' + p[1] + p[2] + ' ' + p[3]

    @_( 'Expression "+" "=" Expression', 'Expression "-" "=" Expression',
        'Expression "*" "=" Expression', 'Expression "/" "=" Expression',
        'Expression MODULUS "=" Expression', 'Expression "&" "=" Expression',
        'Expression "|" "=" Expression', 'Expression "^" "=" Expression',
        'Expression "<" "<" "=" Expression', 'Expression ">" ">" "=" Expression' )      
    def CompoundAssignmentExpression(self, p):
        if len(p) == 4:
            return p[0] + ' ' + p[1] + p[2] + ' ' + p[3]
        elif len(p) == 5:
            return p[0] + ' ' + p[1] + p[2] + p[3] + ' ' + p[4]

    @_( 'Expression "=" Expression' )      
    def AssignmentExpression(self, p):
        return p[0] + ' = ' + p[2]

    @_( 'Expression "=" error' )      
    def AssignmentExpression(self, p):
        msg = 'Error: Invalid assigment. Line ' + str(p.lineno) + '\n'
        self.error_s += msg
        print(msg)

    @_( '"-" Expression %prec UMINUS', '"!" Expression',
        )#'error Expression' )      
    def NegationExpression(self, p):
        return p[0] + p.Expression

    @_( 'KW_RETURN Expression', 'KW_RETURN' )      
    def ReturnExpression(self, p):
        if len(p) == 2:
            return 'return ' + p.Expression
        elif len(p) == 1:
            return 'return'

    @_( 'KW_RETURN error' )      
    def ReturnExpression(self, p):
        msg = 'Error: Invalid return value. Line ' + str(p.lineno) + '\n'
        self.error_s += msg
        print(msg)

    #************************* Types *************************
    @_( 'PrimitiveType' )
    def Type(self, p):
        return p[0]
   
    @_( 'CHAR', 'BOOL', 'NumericType' )
    def PrimitiveType(self, p):
        return p[0]

    #--------------- Floating point types ---------------
    @_( 'F64' )
    def NumericType(self, p):
        return 'long double'

    @_( 'F32' )
    def NumericType(self, p):
        return 'float'

    #--------------- Integer types ---------------
    @_( 'U8', 'U16', 'U32', 'U64' )
    def NumericType(self, p):
        return p[0].replace( 'u', 'uint' ) + '_t'

    @_( 'I8', 'I16', 'I32', 'I64' )
    def NumericType(self, p):
        return p[0].replace( 'i', 'int' ) + '_t'

    @_( 'USIZE', 'ISIZE' )
    def NumericType(self, p):
        if p[0][0] == 'u':
            return'uint16_t'
        else:
            return'int16_t'

    #--------------- Literals ---------------
    @_( 'STRING_LITERAL' )                   
    def LiteralExpression(self, p):
        return p[0]

    @_( 'CHAR_LITERAL' )                   
    def LiteralExpression(self, p):
        if self.exType != '' and self.exType != 'CHAR_LITERAL':
            self.error = 'Type error: it is not a char value.\n'
        else:
            self.exType = 'CHAR_LITERAL'
        return p[0]

    @_( 'TRUE', 'FALSE' )                   
    def LiteralExpression(self, p):
        if self.exType != '' and self.exType != 'BOOLEAN_LITERAL':
            self.error = 'Type error: it is not a boolean value.\n'
        else:
            self.exType = 'BOOLEAN_LITERAL'
        return p[0]

    @_( 'FLOAT_LITERAL' )                   
    def LiteralExpression(self, p):
        if self.exType != '' and self.exType != 'INTEGER_LITERAL' and self.exType != 'FLOAT_LITERAL':
            self.error = 'Type error: it is not a numeric value.\n' 
        else:
            self.exType = 'FLOAT_LITERAL'
        return p[0].replace( '_', '' )   
    
    @_( 'INTEGER_LITERAL' )                   
    def LiteralExpression(self, p):
        if self.exType != '' and self.exType != 'INTEGER_LITERAL' and self.exType != 'FLOAT_LITERAL':
            self.error = 'Type error: it is not a numeric value.\n' 
        elif self.exType != 'FLOAT_LITERAL':
            self.exType = 'INTEGER_LITERAL'
        return p[0].replace( '_', '' )

    @_( 'DEC_LITERAL', 'HEX_LITERAL', 'OCT_LITERAL', 'BIN_LITERAL' )                   
    def INTEGER_LITERAL(self, p):
        return p[0]