from sly import Lexer

#lexic analizer
class sokae_Lexer(Lexer):

    tokens = {  MODULUS,
                IDENTIFIER,                                         #identifiers
                L_BLK_COMMENT, R_BLK_COMMENT,                       #block comments  
                STRING_LITERAL, CHAR_LITERAL,                       #strings and chars literals                                   
                DEC_LITERAL, HEX_LITERAL, OCT_LITERAL, BIN_LITERAL, #numeric and boolean literals (CHECK OCTAL)
                FLOAT_LITERAL, TRUE, FALSE,             
                I8, I16, I32, I64, U8, U16, U32, U64, USIZE, ISIZE, #integer types
                F32, F64, BOOL, CHAR,                               #floating point, boolean and char types
                KW_LET, KW_STATIC,  KW_FN, KW_RETURN,               #and reserved words
                KW_IF, KW_ELSE, KW_LOOP, KW_WHILE,
             }

    #Tokens
    MODULUS     = r'%'
    BOOL        = r'bool'
    CHAR        = r'char'
    USIZE       = r'usize'
    ISIZE       = r'isize'
    U8          = r'u8'
    U16         = r'u16'
    U32         = r'u32'
    U64         = r'u64'
    I8          = r'i8'
    I16         = r'i16'
    I32         = r'i32'
    I64         = r'i64'
    F32         = r'f32'
    F64         = r'f64'
    TRUE        = r'true'
    FALSE       = r'false'

    KW_LET      = r'let'
    KW_FN       = r'fn'
    KW_RETURN   = r'return'
    KW_STATIC   = r'static'
    KW_IF       = r'if'    
    KW_ELSE     = r'else'
    KW_LOOP     = r'loop'
    KW_WHILE    = r'while'

    L_BLK_COMMENT   = r'/\*'
    R_BLK_COMMENT   = r'\*/'
    IDENTIFIER      = r'[a-zA-Z_][a-zA-Z0-9_]*'
    STRING_LITERAL  = r'\".*?\"'
    CHAR_LITERAL    = r'\'.\''
    FLOAT_LITERAL   = r'[0-9_]+\.[0-9_]+'
    HEX_LITERAL     = r'0x[0-9A-Fa-f_]+'
    OCT_LITERAL     = r'0o[0-7_]+'
    BIN_LITERAL     = r'0b[01_]+'
    DEC_LITERAL     = r'[0-9_]+'

    literals = {',', ';', ':', '(', ')', '{', '}', '=', 
                '+', '-', '*', '/', '!', '&', '|', '^', '>', '<'}
    
    ignore = ' \t'

    #NEWL = r'\n'
    
    ignore_comment = r'//.*'    # Ignored pattern

    def __init__(self):
        self.error_s = ''       #error stream

    @_(r'\n+')  #line counting
    def newline(self, t):
        self.lineno += t.value.count('\n')

    def error(self, t):
        self.error_s += 'Error: ilegal character \'' + str(t.value[0]) 
        self.error_s += '\'. Line ' + str(t.lineno) + '\n'
        self.index += 1