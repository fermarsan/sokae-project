#Soporte:
# - Inspirado en la gramática de rust (https://doc.rust-lang.org/stable/reference/introduction.html)
# - Literales enteros en notación binaria, octal, hexadecimal y decimal 
# - Literales de punto flotante, de caracter y booleanas
# - Literales con '_'
# - Declaración de variables locales (let) y globales (static)
# - Declaración de variables booleanas y de caracter
# - Declaración de variables enteras de 8, 16, 32 y 64 bits con y sin signo (también isize y usize)
# - Declaración de variables en punto flotante de 32 y 64 bits
# - Declaración y asignación simultánea
# - Declaración con notación de sufijo
# - Declaración por inferencia de tipo por defecto
# - NO soporta la palabra reservada 'mut' ya que por defecto todas las variables son mutables
# - comentarios de linea y de bloque
# - Asignación de variables
# - negación numérica y booleana
# - operaciones aritméticas y lógicas bit a bit
# - paréntesis en las expresiones
# - operaciones lógicas and y or
# - operaciones de comparación
# - operaciones de asignación compuesta '+=', etc.
# - expresiones de bloque '{ Bloque_de_código }' (se agrega una tabulación al bloque)
# - declaraciónes de funciones y retorno de valores
# - sentencias if-else, loop, while

from sokae_Lexer import sokae_Lexer
from sokae_Parser import sokae_Parser
from os import system
import sys

if len(sys.argv) > 1:

    lexer = sokae_Lexer()        #carga el analizador Léxico
    parser = sokae_Parser()      #y el sintáctico
    
    #carga el archivo
    name = sys.argv[1]
    inFile = open(name,'r')  #abre el archivo de entrada
    program = inFile.read()
    inFile.close()
    
    #analiza el archivo
    for t in lexer.tokenize(program):
        print(t)
    parser.parse(lexer.tokenize(program))     #analiza y ejecuta el programa 
    
    #guarda los archivos de salida
    parser.saveOutput(name.replace('.rs','.c'))

else:
    print('No se especificó un archivo de entrada.\n')
        

    

    
    